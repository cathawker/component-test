# H&B Bundles / React Component Test

Please don't spend more than a few hours on this test, we can talk through where you got up to as part of any interview. Be prepared to talk through what you would have done given more time.

## Setup

1. `npm install`
2. `npm run storybook`
3. `npm test`

## The Task

Create a react component in storybook for the bundles component

### Bundles Component

![Component](bundles.png)

Fetch data from the endpoint `https://hb.demo/products` (The data is mocked in the file Bundles.stories.tsx)

### A/C's

1. The 'total price' should update when products are selected.
2. The button text should reflect the number of selected products.
3. Should closely match the design.
4. Should only show products that are in stock.
5. Console.log the SKUs of the products to add to the basket when clicking the button.

### Food for thought

1. Performance / Suspense
2. Scenarios where the API is offline
3. Testing / TDD

### TODOs / Suggested Improvements

* A proper loading state, i.e. placeholder panel
* Complete styling as per design - checkboxes, '+' between panels, web font
* Proper responsiveness, e.g. change layout on mobile, responsive images
* Improve accessibility with proper labelling for checkbox and buttons / make sure screenreader announces updates when product added or removed
* More complete test coverage - I just wrote a couple to demo approach
* Handle edge cases for text, e.g. singular / plural, what to show on button when there are no items selected?
* If doing this "for real" would be considering reuse and scalability of styling, e.g. reusable subcomponents, variables/theme for fonts etc.
* Again, in the real world would use TypeScript properly, but TS checking was not set up and didn't want to spend time on this as it wasn't going to add much here.
* Would be nice if we could get rid of react act warning when running tests - act is not needed when using react testing library.
