import React, { useState } from 'react';
import styled from '@emotion/styled'

import { Product } from './Product';

const BundlesPanel = styled.div(`
  background-color: #eff6f6;
  padding: 16px 48px;
  border-radius: 32px;
  font-family: helvetica, arial, sans-serif;
  left: 0;
  right: 0;
  display: grid;
  grid-template-columns: 40% 60%;
`)

const BundleSummary = styled.div(`
  grid-column-start: 1;
  grid-column-end: 1;
  font-size: 0.8rem;
  line-height: 1.2rem;
  padding: 48px 32px 48px 0;
`)

const BundleText = styled.p(`
  margin-bottom: 32px;
`)

const Button = styled.button(`
  background-color: #93bc34;
  border: 2px solid #93bc34;
  border-radius: 10px;
  font-weight: bold;
  padding: 16px;
  color: #fff; 
  
  &:focus, 
  &:hover {
    background-color: #abd24f;
    border-color: #abd24f;
  }
`)

const PanelHeading = styled.h2(`
  font-size: 1.2rem;
  margin-top: 0;
  margin-bottom: -0.4rem;
`)

const TotalPrice = styled.p(`
  font-size: 1rem;
  font-weight: bold;
  margin-top: 0;
`)

const ProductList = styled.div(`
  display: flex;
  grid-column-start: 2;
  grid-column-end: 2;
  justify-content: space-between;
  gap: 32px;
`)


export const Bundle = ({promise}) => {
  const { products } = promise.read();
  
  const availableProducts = products.filter((product) => {
    return product.stock > 0;
  });

  const initialSelectedItems = availableProducts.map(product => product.sku);

  const [selectedItems, setSelectedItems] = useState(initialSelectedItems);

  const handleCheckboxUpdate = (sku) => {
    return ((event) => {
      if (event.target.checked) {
        setSelectedItems(selectedItems.concat([sku]))
      } else {
        setSelectedItems(selectedItems.filter(currentSku => sku !== currentSku ))
      }
    });
  }

  return (
    <BundlesPanel>

      <BundleSummary>
        <PanelHeading>
          Frequently bought together
        </PanelHeading>
        <BundleText>Please ensure you read the label of all products purchased as part of this bundle before use.</BundleText>

        <TotalPrice>
          Total Price: £{selectedItems.reduce((total, itemSku) => {
            const selectedProduct = availableProducts.find((product) => {
              return product.sku === itemSku;
            });

            total += parseFloat(selectedProduct.price);
            return total;
          }, 0)}
        </TotalPrice>

        <Button onClick={() => console.log(selectedItems)}>
          Add {selectedItems.length} item{ selectedItems.length > 1 && 's'} to basket
        </Button>
      </BundleSummary>
      <ProductList>
      {availableProducts.map((product) => (
        <Product
          product={product} 
          handleCheckboxUpdate={handleCheckboxUpdate(product.sku)} 
          isSelected={selectedItems.includes(product.sku)}
          key={product.sku}
        />
      ))}
      </ProductList>
    </BundlesPanel> 
  )
}
