import React from 'react';
import styled from '@emotion/styled'

const ProductCard = styled.div(`
  background-color: #fff;
  border-radius: 10px;
  padding: 8px;
  position: relative;
  font-size: 0.8rem;
  line-height: 1.2rem;
`);

const ProductImage = styled.img(`
  max-width: 100%;
  height: auto;
  display: block;
  margin: 0 auto 10px auto;
`)

const ProductCheckbox = styled.input(`
  position: absolute;
  top: 10px;
  right: 10px;
`)

const ProductDescription = styled.div(`
  font-weight: bold;
  margin-bottom: 8px;
`)

export const Product = ({
  product: {
    id,
    image,
    sku,
    title,
    price,
    stock
  },
  handleCheckboxUpdate,
  isSelected
}) => {
  return (
    <ProductCard data-testid="product">
      <ProductImage src={`${image}`} alt="" />
      <ProductCheckbox type="checkbox" checked={isSelected} onChange={handleCheckboxUpdate} data-testid={`sku-${sku}`} />
      <ProductDescription>{title}</ProductDescription>
      <div>£{price}</div>
    </ProductCard>
  )
}