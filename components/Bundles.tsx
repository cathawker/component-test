import React, { useEffect, useState, Suspense } from 'react';
import axios from 'axios';

import suspensify from '../lib/suspensify';

import { Bundle } from './Bundle';

function fetchData() {
  return axios('https://hb.demo/products');
}

const Loading = () => {
  return (<p>LOADING...</p>)
}

export const Bundles = () => {
  const promise = suspensify(fetchData());
  
  return (
    <div>
      <Suspense fallback={<Loading />}>
        <Bundle promise={promise} />
      </Suspense>
    </div>
  );
};
